package com.example.service;

import com.example.model.po.UserRoleLink;

public interface IUserRoleLinkService extends IBaseService<UserRoleLink> {
}
