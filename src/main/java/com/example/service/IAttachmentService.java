package com.example.service;

import com.example.model.po.Attachment;

public interface IAttachmentService extends IBaseService<Attachment> {
}
