package com.example.service;

import com.example.model.po.MailAttachmentLink;

public interface IMailAttachmentLinkService extends IBaseService<MailAttachmentLink> {
}
