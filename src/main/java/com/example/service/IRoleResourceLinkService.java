package com.example.service;

import com.example.model.po.RoleResourceLink;

public interface IRoleResourceLinkService extends IBaseService<RoleResourceLink> {
}
