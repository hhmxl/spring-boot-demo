package com.example.service;

import com.example.model.po.MailContent;

public interface IMailContentService extends IBaseService<MailContent> {
}
