package com.example.service;

import com.example.model.po.Role;

public interface IRoleService extends IBaseService<Role> {

    int DEFAULT = 1;

}
