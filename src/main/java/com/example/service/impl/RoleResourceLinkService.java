package com.example.service.impl;

import com.example.mapper.RoleResourceLinkMapper;
import com.example.model.po.RoleResourceLink;
import com.example.service.IRoleResourceLinkService;
import org.springframework.stereotype.Service;

@Service
public class RoleResourceLinkService extends BaseService<RoleResourceLinkMapper, RoleResourceLink> implements IRoleResourceLinkService {
}
