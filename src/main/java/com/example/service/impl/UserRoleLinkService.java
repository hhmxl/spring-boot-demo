package com.example.service.impl;

import com.example.mapper.UserRoleLinkMapper;
import com.example.model.po.UserRoleLink;
import com.example.service.IUserRoleLinkService;
import org.springframework.stereotype.Service;

@Service
public class UserRoleLinkService extends BaseService<UserRoleLinkMapper, UserRoleLink> implements IUserRoleLinkService {
}
