package com.example.service;

import com.example.model.po.JobParameter;

public interface IJobParameterService extends IBaseService<JobParameter> {
}
