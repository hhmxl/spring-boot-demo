package com.example.service;

import com.example.model.po.MailToUserLink;

public interface IMailToUserLinkService extends IBaseService<MailToUserLink> {
}
