package com.example.service;

import com.example.model.po.JobTemplateParameter;

public interface IJobTemplateParameterService extends IBaseService<JobTemplateParameter> {
}
