package com.example.service;

import com.example.model.po.BroadcastToUserLink;

public interface IBroadcastToUserLinkService extends IBaseService<BroadcastToUserLink> {
}
