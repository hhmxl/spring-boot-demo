package com.example.service;

import com.example.model.po.JobTemplate;

public interface IJobTemplateService extends IBaseService<JobTemplate> {
}
