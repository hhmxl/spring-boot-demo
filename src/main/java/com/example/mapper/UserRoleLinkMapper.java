package com.example.mapper;

import com.example.model.po.UserRoleLink;

public interface UserRoleLinkMapper extends IBaseMapper<UserRoleLink> {
}
