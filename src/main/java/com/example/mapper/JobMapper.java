package com.example.mapper;

import com.example.model.po.Job;

public interface JobMapper extends IBaseMapper<Job> {
}
