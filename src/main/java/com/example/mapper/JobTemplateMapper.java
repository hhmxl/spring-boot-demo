package com.example.mapper;

import com.example.model.po.JobTemplate;

public interface JobTemplateMapper extends IBaseMapper<JobTemplate> {
}
