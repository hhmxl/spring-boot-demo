package com.example.mapper;

import com.example.model.po.BroadcastToUserLink;

public interface BroadcastToUserLinkMapper extends IBaseMapper<BroadcastToUserLink> {
}
