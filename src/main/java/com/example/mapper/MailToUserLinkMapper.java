package com.example.mapper;

import com.example.model.po.MailToUserLink;

public interface MailToUserLinkMapper extends IBaseMapper<MailToUserLink> {
}
