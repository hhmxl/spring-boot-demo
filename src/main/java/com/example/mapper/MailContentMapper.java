package com.example.mapper;

import com.example.model.po.MailContent;

public interface MailContentMapper extends IBaseMapper<MailContent> {
}
