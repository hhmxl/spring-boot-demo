package com.example.mapper;

import com.example.model.po.User;

public interface UserMapper extends IBaseMapper<User> {
}
