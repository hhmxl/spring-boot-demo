package com.example.mapper;

import com.example.model.po.JobParameter;

public interface JobParameterMapper extends IBaseMapper<JobParameter> {
}
