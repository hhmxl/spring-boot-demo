package com.example.mapper;

import com.example.model.po.JobTemplateParameter;

public interface JobTemplateParameterMapper extends IBaseMapper<JobTemplateParameter> {
}
