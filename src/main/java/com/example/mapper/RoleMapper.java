package com.example.mapper;

import com.example.model.po.Role;

public interface RoleMapper extends IBaseMapper<Role> {
}
