package com.example.mapper;

import com.example.model.po.Attachment;

public interface AttachmentMapper extends IBaseMapper<Attachment> {
}
