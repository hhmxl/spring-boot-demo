package com.example.mapper;

import com.example.model.po.Dept;

public interface DeptMapper extends IBaseMapper<Dept> {
}
