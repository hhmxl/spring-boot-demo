package com.example.mapper;

import com.example.model.po.RoleResourceLink;

public interface RoleResourceLinkMapper extends IBaseMapper<RoleResourceLink> {
}
