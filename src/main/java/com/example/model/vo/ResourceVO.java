package com.example.model.vo;

import com.example.model.po.Resource;
import lombok.Data;

@Data
public class ResourceVO extends Resource {

    private static final long serialVersionUID = 1L;

}
