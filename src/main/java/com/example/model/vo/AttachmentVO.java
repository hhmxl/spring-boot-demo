package com.example.model.vo;

import com.example.model.po.Attachment;
import lombok.Data;

@Data
public class AttachmentVO extends Attachment {

    private static final long serialVersionUID = 1L;

}
