package com.example.model.vo;

import com.example.model.po.JobTemplateParameter;
import lombok.Data;

@Data
public class JobTemplateParameterVO extends JobTemplateParameter {

    private static final long serialVersionUID = 1L;

}
