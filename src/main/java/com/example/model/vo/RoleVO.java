package com.example.model.vo;

import com.example.model.po.Role;
import lombok.Data;

@Data
public class RoleVO extends Role {

    private static final long serialVersionUID = 1L;

}
