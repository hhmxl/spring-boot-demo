package com.example.model.vo;

import com.example.model.po.Mail;
import lombok.Data;

@Data
public class MailVO extends Mail {

    private static final long serialVersionUID = 1L;

}
