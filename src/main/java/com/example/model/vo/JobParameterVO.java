package com.example.model.vo;

import com.example.model.po.JobParameter;
import lombok.Data;

@Data
public class JobParameterVO extends JobParameter {

    private static final long serialVersionUID = 1L;

}
