package com.example.model.vo;

import com.example.model.po.Dept;
import lombok.Data;

@Data
public class DeptVO extends Dept {

    private static final long serialVersionUID = 1L;

}
