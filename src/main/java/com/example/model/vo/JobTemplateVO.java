package com.example.model.vo;

import com.example.model.po.JobTemplate;
import lombok.Data;

@Data
public class JobTemplateVO extends JobTemplate {

    private static final long serialVersionUID = 1L;

}
