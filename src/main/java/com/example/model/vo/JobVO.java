package com.example.model.vo;

import com.example.model.po.Job;
import lombok.Data;

@Data
public class JobVO extends Job {

    private static final long serialVersionUID = 1L;

}
