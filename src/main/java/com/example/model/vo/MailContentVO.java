package com.example.model.vo;

import com.example.model.po.MailContent;
import lombok.Data;

@Data
public class MailContentVO extends MailContent {

    private static final long serialVersionUID = 1L;

}
